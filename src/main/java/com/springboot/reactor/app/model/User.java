package com.springboot.reactor.app.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class User {

	@Getter @Setter
	private String nombre;
	
	@Getter @Setter
	private String apellido;
	
	@Getter @Setter
	private int edad;

}
