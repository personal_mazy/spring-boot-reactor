package com.springboot.reactor.app.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Comentarios {

	@Getter
	@Setter
	public List<String> comentarios;

	public void addcomentarios(String coment) {
		if (null != coment) {
			comentarios.add(coment);
		}
	}
}
