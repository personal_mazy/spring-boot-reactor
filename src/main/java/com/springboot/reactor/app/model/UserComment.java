package com.springboot.reactor.app.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserComment {

	@Getter
	@Setter
	private User usuario;
	
	@Getter
	@Setter
	private Comentarios comentarios;
	
}
