package com.springboot.reactor.app;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.springboot.reactor.app.model.Comentarios;
import com.springboot.reactor.app.model.User;
import com.springboot.reactor.app.model.UserComment;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@SpringBootApplication
public class SpringBootReactorApplication implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(SpringBootReactorApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootReactorApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		ejemploCarga();
	}
	
	public void ejemploCarga() {
		Flux.range(1, 10)
		.log()
		//.limitRate(5)// este metodo reduce la implementacion del subscriptor de abajo (Subscriber) pero se tiene menos control
		.subscribe(new Subscriber<Integer>() {
		
			private Subscription s;
			private Integer limite= 5;
			private Integer consumido=0;
			
			@Override
			public void onSubscribe(Subscription s) {
				this.s=s;
				s.request(this.limite);
			}

			@Override
			public void onNext(Integer t) {
				log.info(t.toString());
				this.consumido++;
				if(consumido==limite) {
					consumido=0;
					s.request(this.limite); // volviendo a solicitar mas elementos al acabar la primera peticion o x lotes
				}
			}

			@Override
			public void onError(Throwable t) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onComplete() {
				// TODO Auto-generated method stub
				
			}
		}
		);
	}
	
	public void ejemploIntervFromCreate() {
		Flux.create( e-> {
			Timer t= new Timer();
			t.schedule(new TimerTask() {
				
				private Integer c=0;
				
				@Override
				public void run() {
					e.next(++c);
					if(c==10) {
						t.cancel();
						e.complete(); // se cancela el flujo
					}
					if(c==5) {
						e.error(new InterruptedException("Ocurrio un error en el 5"));
					}
				}
			}, 1000, 1000);
		})
		//.doOnNext( n-> log.info(n.toString()))
		//.doOnComplete(()-> log.info("Se completo el flujo!!!!"))
		//el complete solo se llama cuando se completa el flujo el error SIN ERRORES
		.subscribe(next -> log.info(next.toString()), error -> log.error(error.getMessage()),() -> log.info("Se completo el flujo!!!!"));
		
	}
	
	public void ejemploIntervInfinito() {
		
		CountDownLatch latch = new CountDownLatch(1);
		
		Flux.interval(Duration.ofSeconds(1))
		.doOnTerminate(() -> latch.countDown()) // este metodo decrementa en 1 el countDownWatch
		.flatMap( i-> {
			if(i>=5) {
				return Flux.error(new InterruptedException("Solo valores menores a 5!!"));
			}
			return Flux.just(i);
		})
		.map(i -> "Valor: "+i)
		.retry(2) // si sale error, intentara lanzar 2 veces mas el flujo
		.subscribe( a -> log.info(a), e -> log.error(e.getMessage()));
		
		//espera q el countdownb sea 0
		try {
			latch.await();
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	
	public void ejemploInterval() {
		Flux<Integer> rango = Flux.range(1, 12);
		Flux<Long> retraso = Flux.interval(Duration.ofSeconds(1));
		
		//cuando se invoca al metodo subscribe crea hilos donde se estan ejecutando en paralelo por ese motivo no se vera en la consola
		// si se quiere ver en la consolo habria que ponerlo como flujo bloqueante aunq no es recomendable, se subscribe al flujo con bloqueo el metodo blocklast()
		// y se desbloquea cuando termina el observable
		rango.zipWith(retraso, (a,b) -> a).doOnNext(i -> log.info(i.toString()))
		.blockLast();
		//.subscribe();
		
	}
	
	public void ejemploDelay() {
		Flux<Integer> rango = Flux.range(1, 12).delayElements(Duration.ofSeconds(1))
				.doOnNext(i -> log.info(i.toString()));
		rango.blockLast();
		
		// al igual que arriba, no lo bloqueara, aunq si se duerme el hilo principal por 13 seg si se vera la ejecucion
		/*rango.subscribe();
		try {
			Thread.sleep(13000);
		}catch (Exception e) {
			
		}*/
	}
	
	public void exampleRange() {
		Flux<Integer> fi = Flux.range(0, 4);
		Flux.just(1,2,3,4).map(x -> (x*2)).zipWith(fi ,(a,b)-> String.format("Primer Flix: %d, Segundo Flux :%d", a,b))
		.subscribe( log::info); 
	}
	
	public void exampleUserCommentZipWithF2() {
		Mono<User> monoUser = Mono.fromCallable(() -> new User("Alonso", "ZY", 29));
		
		Mono<Comentarios> monoC = Mono.fromCallable(() -> {
			Comentarios c = new Comentarios();
			c.setComentarios(new ArrayList<>());
			c.addcomentarios("Hola Jenn!");
			c.addcomentarios("Mñn gogo");
			return c;
		});
		
		Mono<UserComment> mUC= monoUser.zipWith(monoC).map(tuple -> {
			//las tuplas contienen los elementos que se estan combinando
			//ESTA ES LA SEGUNDA FORMA DE COMBINAR DOS FLUJOS
			User u= tuple.getT1();
			Comentarios c = tuple.getT2();
			return new UserComment(u, c);
		});
		mUC.subscribe(v -> log.info("ver: "+v.toString()));
		
	}
	
	public void exampleUserCommentZipWith() {
		Mono<User> monoUser = Mono.fromCallable(() -> new User("Alonso", "ZY", 29));
		
		Mono<Comentarios> monoC = Mono.fromCallable(() -> {
			Comentarios c = new Comentarios();
			c.setComentarios(new ArrayList<>());
			c.addcomentarios("Hola Jenn!");
			c.addcomentarios("Mñn gogo");
			return c;
		});
		
		Mono<UserComment> mUC= monoUser.zipWith(monoC, (user, com) -> new UserComment(user, com));
		mUC.subscribe(v -> log.info("ver: "+v.toString()));
		
	}
	
	
	public void exampleUserComment() {
		Mono<User> monoUser = Mono.fromCallable(() -> new User("Alonso", "ZY", 29));
		
		Mono<Comentarios> monoC = Mono.fromCallable(() -> {
			Comentarios c = new Comentarios();
			c.setComentarios(new ArrayList<>());
			c.addcomentarios("Hola Jenn!");
			c.addcomentarios("Mñn gogo");
			return c;
		});
		
		monoUser.flatMap(u -> monoC.map(c -> new UserComment(u, c))).subscribe(
				uc -> log.info("Combinacion: "+uc.toString()));
	}
	
	public void exampleCollect() {
		List<User> users_ini= new ArrayList<User>();
		users_ini.add(new User("Cuti", "", 7));
		users_ini.add(new User("Peque","", 1));
		users_ini.add(new User("Ringo","", 3));
		users_ini.add(new User("Charlie","", 10));
		users_ini.add(new User("Tazan","", 9));
		
		Flux.fromIterable(users_ini).collectList(). // este metodo convierte de un flux a un tipo Mono // mono: emite un sola vez por todo, flux : emite elemento por elemento
		subscribe(lista -> /*log.info(lista.toString()*/
					{
						lista.forEach(item -> log.info("Elemento: "+item.toString()));
					});
	}
	
	public void exampleToString() {
		List<User> users_ini= new ArrayList<User>();
		users_ini.add(new User("Cuti", "", 7));
		users_ini.add(new User("Peque","", 1));
		users_ini.add(new User("Ringo","", 3));
		users_ini.add(new User("Charlie","", 10));
		users_ini.add(new User("Tazan","", 9));
		
		Flux.fromIterable(users_ini).map(usuario -> usuario.getNombre().toUpperCase().concat(" - Edad: "+usuario.getEdad())
				)
				.flatMap(user -> {
					if(user.contains("U")) {
						return Mono.just(user);
					}else {
						return Mono.empty();
					}
				})
				.map(el -> {
					return el.toLowerCase();
				}).subscribe(e -> log.info(e.toString()));
	}
	
	public void exampleFlapMap() {
		List<String> users_ini= new ArrayList<String>();
		users_ini.add("Cuti 7");
		users_ini.add("Peque 1");
		users_ini.add("Ringo 3");
		users_ini.add("Charlie 10");
		users_ini.add("Tazan 9");
		
		Flux.fromIterable(users_ini).map(el ->
					new User(el.split(" ")[0].toUpperCase(), "", Integer.parseInt(el.split(" ")[1]))
				)
				.flatMap(user -> {
					if(user.getEdad()>6) {
						return Mono.just(user);
					}else {
						return Mono.empty();
					}
				})
				.map(el -> {
					el.setNombre(el.getNombre().toLowerCase());
					return el;
				}).subscribe(e -> log.info(e.toString()));
	}

	public void exampleIterable() {
		List<String> users_ini= new ArrayList<String>();
		users_ini.add("Cuti 7");
		users_ini.add("Peque 1");
		users_ini.add("Ringo 3");
		users_ini.add("Charlie 10");
		users_ini.add("Tazan 9");
		
		// creando el primer observable
		//creando un stream reactivo a traves de un iterable
		Flux<String> nombres = Flux.fromIterable(users_ini); //Flux.just("cuti 7", "peque 1", "ringo 3", "Charlie 10","tarzan 9");
		//los streans son inmutables, los subscriptores leeran el  original, mas no las operaciones que se hagan con el flux ya que retorna una nueva instancia de flux
		
		Flux<User> users=nombres.map(el -> //return el.toUpperCase();
					new User(el.split(" ")[0].toUpperCase(), "", Integer.parseInt(el.split(" ")[1]))
				)
				.filter(user -> user.getEdad() > 0)
				.doOnNext(/*elemento -> System.out.println(elemento)*/
						e -> {
							if (e==null) {
								throw new RuntimeException("No puede estar vacio el NOMBRE");
							}
							System.out.println(e.toString());
							
						})
				.map(el -> {
					el.setNombre(el.getNombre().toLowerCase());
					//el tipo de objeto que se retorna es el tipo de flux
					return el;
				});
		
		//suscribiendose al observable, manejando errores del observable
		users.subscribe(/*log::info*/
				e-> log.info(e.toString()), error -> log.error(error.getMessage()), new Runnable() {
					
					@Override
					public void run() {
						log.info("SE ACABOOOOO");
					}
				});
	}
}
